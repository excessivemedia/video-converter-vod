#!/usr/bin/env bash
[ -f "${2}" ] && exit 0
ffmpeg -i "${1}" -pix_fmt yuv420p -deinterlace -vf "scale=1280:720" -threads 0 -vcodec h264 -r 50 -g 33 -sc_threshold 0 -b:v 3600k -minrate:v 3600k -maxrate:v 3600k -muxrate 4000k -bufsize:v 1200k -bf 2 -preset medium -tune film -acodec mp3 -b:a 192k -ac 2 -ar 48000 -af "aresample=async=1:min_hard_comp=0.100000:first_pts=0" -f mp4 "${2}"

