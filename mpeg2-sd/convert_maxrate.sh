#!/usr/bin/env bash
[ -f "${2}" ] && exit 0
ffmpeg -i "${1}" -pix_fmt yuv420p -deinterlace -vf "scale=720:576" -threads 0 -vcodec mpeg2video -r 25 -g 12 -sc_threshold 0 -b:v 3200k -minrate:v 3200k -maxrate:v 3200k -bf 2 -bufsize:v 1200k -preset medium -tune film -acodec mp3 -b:a 192k -ac 2 -ar 48000 -af "aresample=async=1:min_hard_comp=0.100000:first_pts=0" -f mpegts "${2}"

