#!/bin/bash

EXT="mpeg m2t avi mov"
M2T=$(find ./* -type d -name \*-sd -maxdepth 0 2>/dev/null)
MP4=$(find ./* -type d -name \*-hdr -maxdepth 0 2>/dev/null)
OWD=$(pwd)

for format in ${M2T}; do
  cd "${OWD}"
  if [ -f "${format}/convert.sh" ]; then
    cd "${format}"
    for ext in $EXT; do
      for file in $(find input -type f -name \*.$ext); do
        file=$(basename "${file}")
        ./convert.sh "input/${file}" "output/${file%.$ext}.m2t"
      done
    done
  fi
done

for format in ${MP4}; do
  cd "${OWD}"
  if [ -f "${format}/convert.sh" ]; then
    cd "${format}"
    for ext in $EXT; do
      for file in $(find input -type f -name \*.$ext); do
        file=$(basename "${file}")
        ./convert.sh "input/${file}" "output/${file%.$ext}.mp4"
      done
    done
  fi
done
